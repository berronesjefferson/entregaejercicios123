import React from "react";
import { Contacto } from "../../models/Contacto.class";
import ContactoComponent from "../pure/Contacto";

const ContactoListComponent = () => {
  const [contact, setContact] = React.useState(
    new Contacto("Jefferson Raul", "Berrones Chimbolema", "berronesjefferson@gmail.com", false)
  );

  const changeState = () => {
    setContact((current) => ({ ...contact, conectado: !current.conectado }));
  };

  return (
    <div>
      <div>
        <h1>Your Contactos:</h1>
      </div>
      <ContactoComponent contacto={contact}></ContactoComponent>
      <button type="button" onClick={changeState} className="button-78">
        Cambiar mi estado
      </button>
    </div>
  );
};

export default ContactoListComponent;
