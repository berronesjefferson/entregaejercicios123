import logo from "./logo.svg";
import "./App.css";
import ContactoListComponent from "./components/container/ContactoList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ContactoListComponent />
      </header>
    </div>
  );
}

export default App;
